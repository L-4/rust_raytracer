use tracer::camera::Camera;
use tracer::color::Color;
use tracer::material::Material;
use tracer::object::sphere::Sphere;
use tracer::rendersettings::RenderSettings;
use tracer::world::World;

use cgmath::Vector3;

use indicatif::{ProgressBar, ProgressStyle};

fn main() {
    let mut world = World::new();

    &world
        .push(Sphere::new(
            Vector3::new(0.0, 0.0, -1.0),
            0.5,
            Material::new(0.5, 1.0, Color::new(1.0, 0.2, 0.2)),
        ))
        .push(Sphere::new(
            Vector3::new(0.0, -100.5, -1.0),
            100.0,
            Material::new(0.1, 0.8, Color::new(0.8, 0.8, 0.0)),
        ))
        .push(Sphere::new(
            Vector3::new(0.0, 100.5, -1.0),
            100.0,
            Material::new(0.1, 0.0, Color::new(0.8, 0.8, 0.0)),
        ))
        .push(Sphere::new(
            Vector3::new(1.0, 0.0, -1.0),
            0.5,
            Material::new(1.0, 0.3, Color::new(0.1, 0.1, 0.8)),
        ))
        .push(Sphere::new(
            Vector3::new(-1.0, 0.0, -1.0),
            0.5,
            Material::new(1.0, 0.0, Color::new(0.7, 0.5, 0.2)),
        ));

    let camera = Camera::new();

    let render_settings = RenderSettings {
        filename: "camera_tests.png".to_string(),
        width: 1440,
        height: 720,
        samples: 25,
        max_reflections: 4,
    };

    let progress_bar = ProgressBar::new(render_settings.width as u64);
    progress_bar.set_style(
        ProgressStyle::default_bar()
            .template(&format!(
                "{{prefix:.bold}}▕{{wide_bar:.red}}▏{{eta_precise}}"
            ))
            .progress_chars("█▉▊▋▌▍▎▏  "),
    );
    progress_bar.set_draw_delta((render_settings.width as u64) / 1500);
    progress_bar.tick();

    progress_bar.set_prefix(&"Rendering single frame".to_string());
    camera.render(&world, &render_settings, || {
        progress_bar.inc(1);
    });
    progress_bar.finish_with_message("Complete!");
}
