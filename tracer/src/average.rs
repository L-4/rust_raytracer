const ROLLINGBUFFER_SIZE: usize = 16;

pub struct RollingBuffer {
	writer: usize,
	data: [f64; ROLLINGBUFFER_SIZE],
	pub average: f64,
}

impl RollingBuffer {
	pub fn new() -> RollingBuffer {
		RollingBuffer {
			writer: 0,
			data: [0.0; ROLLINGBUFFER_SIZE],
			average: 0.0,
		}
	}

	pub fn push(&mut self, data: f64) {
		self.average += (data - self.data[self.writer]) / ROLLINGBUFFER_SIZE as f64;
		self.data[self.writer] = data;
		self.writer = (self.writer + 1) % ROLLINGBUFFER_SIZE;
	}
}

// LargeAverage

pub struct LargeAverage {
	pub average: f64,
	pub index: u32,
}

impl LargeAverage {
	pub fn new() -> LargeAverage {
		LargeAverage {
			average: 0.0,
			index: 1,
		}
	}

	pub fn push(&mut self, data: f64) {
		// TODO: untested
		self.average += (data - self.average) / self.index as f64;
		self.index += 1;
	}
}
