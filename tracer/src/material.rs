use crate::color::Color;
use crate::ray::Ray;
use crate::traceresult::TraceResult;

use cgmath::InnerSpace;
use cgmath::Vector3;

use rand::Rng;

pub struct Material {
	pub metalness: f64,
	pub roughness: f64,
	pub albedo: Color,
}

fn random_in_unit_sphere() -> Vector3<f64> {
	let mut generator = rand::thread_rng(); // TODO: move this out of function
	loop {
		let vec = 2.0
			* Vector3::new(
				generator.gen::<f64>(),
				generator.gen::<f64>(),
				generator.gen::<f64>(),
			) - Vector3::new(1.0, 1.0, 1.0);

		if vec.magnitude() < 1.0 {
			return vec.normalize();
		}
	}
}

impl Material {
	pub fn new(metalness: f64, roughness: f64, albedo: Color) -> Material {
		Material {
			metalness,
			roughness,
			albedo,
		}
	}

	pub fn scatter(&self, trace_result: &TraceResult, ray: &Ray) -> Ray {
		let scattered_delta = trace_result.normal + random_in_unit_sphere();
		let reflected_delta =
			ray.direction - (2.0 * ray.direction.dot(trace_result.normal)) * trace_result.normal;
		Ray::new(
			trace_result.position,
			self.roughness * scattered_delta + (1.0 - self.roughness) * reflected_delta,
		)
	}
}
