use crate::color::Color;
use crate::object::Hitable;
use crate::ray::Ray;
use crate::rendersettings::RenderSettings;
use crate::traceresult::TraceResult;

use cgmath::InnerSpace;

/// World containing a list of hitable items
pub struct World {
	list: Vec<Box<dyn Hitable>>,
}

impl World {
	pub fn new() -> World {
		World { list: Vec::new() }
	}

	pub fn push<T: Hitable + 'static>(&mut self, item: T) -> &mut World {
		self.list.push(Box::new(item));
		self
	}

	fn skybox(&self, ray: &Ray) -> Color {
		let t = 0.5 * (1.0 + ray.direction.normalize().y);
		(1.0 - t) * Color::one() + t * Color::new(0.5, 0.7, 1.0)
	}

	/// Initiates get_color with first ray
	pub fn color_at(&self, ray: &Ray, render_settings: &RenderSettings) -> Color {
		self.color(&ray, render_settings.max_reflections - 1)
	}

	fn color(&self, ray: &Ray, depth: u32) -> Color {
		let mut hit_result: Option<(&Box<dyn Hitable>, TraceResult)> = None;
		let mut closest_so_far = std::f64::MAX;

		for hitable in &self.list {
			if let Some(trace_result) = hitable.hit(&ray, 0.0001, closest_so_far) {
				hit_result = Some((hitable, trace_result));
				closest_so_far = trace_result.distance;
			}
		}

		if let Some(result) = hit_result {
			let (hitable, trace_result) = result;
			let material = hitable.material();
			if depth == 0 {
				material.albedo
			} else {
				let new_ray = material.scatter(&trace_result, &ray);
				material.albedo * self.color(&new_ray, depth - 1) * 0.99
			}
		} else {
			self.skybox(&ray)
		}
	}
}
