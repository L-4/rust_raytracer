use cgmath::Vector3;

use crate::color::Color;
use crate::ray::Ray;
use crate::rendersettings::RenderSettings;
use crate::world::World;

use image::GenericImage;
use rand::Rng;

pub struct Camera {
	origin: Vector3<f64>,
	horizontal: Vector3<f64>,
	vertical: Vector3<f64>,
	lower_left: Vector3<f64>,
}

impl Camera {
	fn get_ray(&self, u: f64, v: f64) -> Ray {
		Ray::new(
			self.origin,
			self.lower_left + u * self.horizontal + v * self.vertical - &self.origin,
		)
	}

	pub fn new() -> Camera {
		Camera {
			origin: Vector3::new(0.0, 0.0, 0.0),
			horizontal: Vector3::new(4.0, 0.0, 0.0),
			vertical: Vector3::new(0.0, 2.0, 0.0),
			lower_left: Vector3::new(-2.0, -1.0, -1.0),
		}
	}

	pub fn render<F>(&self, world: &World, render_settings: &RenderSettings, on_render_column: F)
	where
		F: Fn(),
	{
		let mut image = image::DynamicImage::new_rgb8(
			// TODO: there might be an iterator for creating images
			render_settings.width,
			render_settings.height,
		);

		let mut generator = rand::thread_rng();

		for x in 0..render_settings.width {
			for y in 0..render_settings.height {
				let mut color_accumulator = Color::zero();
				for _i in 0..render_settings.samples {
					let ray = self.get_ray(
						(x as f64 + generator.gen::<f64>()) / render_settings.width as f64,
						(y as f64 + generator.gen::<f64>()) / render_settings.height as f64,
					);

					let new_color = world.color_at(&ray, render_settings);
					color_accumulator += new_color;
				}
				color_accumulator /= render_settings.samples as f64;
				image.put_pixel(
					x,
					render_settings.height - 1 - y,
					image::Rgba(color_accumulator.to_u8_array()),
				);
			}
			on_render_column();
		}
		image.save(&render_settings.filename).unwrap();
	}
}
