pub mod object;
pub mod ray;
//pub mod hitrecord;
//pub mod scene;
pub mod average;
pub mod camera;
pub mod color;
pub mod material;
pub mod rendersettings;
pub mod traceresult;
pub mod world;

// Camera.render(&world)
// Camera generates prime rays
// World receives rays and returns colors

// World.color_at(ray)
// Traces against all hitables
// Gets one hitable
// Hitable receives old ray, old color, hit info
// Hitable returns new ray, new color
// When max depth or sky (nothing) hit, returns color
// Otherwise traces again. CALLREL -6
