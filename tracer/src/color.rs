#[derive(Copy, Clone, Debug)]
pub struct Color {
	r: f64,
	g: f64,
	b: f64,
}

impl Color {
	pub fn new(r: f64, g: f64, b: f64) -> Color {
		Color { r, g, b }
	}

	pub fn one() -> Color {
		Color {
			r: 1.0,
			g: 1.0,
			b: 1.0,
		}
	}

	pub fn zero() -> Color {
		Color {
			r: 0.0,
			g: 0.0,
			b: 0.0,
		}
	}

	pub fn to_u8_array(&self) -> [u8; 4] {
		[
			(self.r.sqrt() * 255.0) as u8,
			(self.g.sqrt() * 255.0) as u8,
			(self.b.sqrt() * 255.0) as u8,
			255 as u8,
		]
	}

	pub fn rgb_sum(&self) -> f64 {
		self.r + self.g + self.b
	}
}

use std::ops::{Add, AddAssign, Div, DivAssign, Mul};

// Mul

impl Mul<Color> for Color {
	type Output = Self;
	fn mul(self, rhs: Self) -> Self {
		Color {
			r: self.r * rhs.r,
			g: self.g * rhs.g,
			b: self.b * rhs.b,
		}
	}
}

impl Mul<f64> for Color {
	type Output = Color;
	fn mul(self, other: f64) -> Color {
		Color {
			r: self.r * other,
			g: self.g * other,
			b: self.b * other,
		}
	}
}

impl Mul<Color> for f64 {
	type Output = Color;
	fn mul(self, other: Color) -> Color {
		Color {
			r: other.r * self,
			g: other.g * self,
			b: other.b * self,
		}
	}
}

impl Mul<Color> for &mut Color {
	type Output = Color;
	fn mul(self, other: Color) -> Color {
		Color {
			r: self.r * other.r,
			g: self.g * other.g,
			b: self.b * other.b,
		}
	}
}

// Div

impl Div<f64> for Color {
	type Output = Color;
	fn div(self, other: f64) -> Self {
		Color {
			r: self.r / other,
			g: self.g / other,
			b: self.b / other,
		}
	}
}

impl Div<f64> for &mut Color {
	type Output = Color;
	fn div(self, other: f64) -> Color {
		Color {
			r: self.r / other,
			g: self.g / other,
			b: self.b / other,
		}
	}
}

impl DivAssign<f64> for Color {
	fn div_assign(&mut self, rhs: f64) {
		*self = *self / rhs;
	}
}

// Add

impl Add for Color {
	type Output = Self;
	fn add(self, rhs: Self) -> Self {
		Color {
			r: self.r + rhs.r,
			g: self.g + rhs.g,
			b: self.b + rhs.b,
		}
	}
}

impl Add<Color> for &mut Color {
	type Output = Color;
	fn add(self, other: Color) -> Color {
		Color {
			r: self.r + other.r,
			g: self.g + other.g,
			b: self.b + other.b,
		}
	}
}

impl AddAssign for Color {
	fn add_assign(&mut self, rhs: Self) {
		*self = *self + rhs;
	}
}
