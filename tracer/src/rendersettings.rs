pub struct RenderSettings {
	pub width: u32,
	pub height: u32,
	pub filename: String,
	// Graphical quality
	pub samples: u32,
	pub max_reflections: u32,
}
