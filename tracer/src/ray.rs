use cgmath::InnerSpace;
use cgmath::Vector3;

#[derive(Debug, Copy, Clone)]
pub struct Ray {
	pub origin: Vector3<f64>,
	pub direction: Vector3<f64>,
}

impl Ray {
	pub fn new(origin: Vector3<f64>, direction: Vector3<f64>) -> Ray {
		Ray {
			origin,
			direction: direction.normalize(),
		}
	}
	pub fn point_at_parameter(&self, distance: f64) -> Vector3<f64> {
		self.origin + self.direction * distance
	}
}
