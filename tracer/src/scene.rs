use image::GenericImage;

use cgmath::prelude::InnerSpace;
use cgmath::prelude::*;
use cgmath::Vector3;

use crate::camera::Camera;
use crate::hitrecord::HitRecord;
use crate::object::hitablelist::HitableList;
use crate::object::Hitable;
use crate::ray::Ray;

use rand::Rng;

pub struct Scene {
	hitable_list: HitableList,
}

impl Scene {
	pub fn new(hitable_list: HitableList) -> Scene {
		Scene { hitable_list }
	}

	pub fn color(&self, ray: &Ray, depth: u16) -> Vector3<f64> {
		let mut hit_record = HitRecord::new();

		if let Some(new_ray) = self
			.hitable_list
			.hit(&ray, 0.0001, std::f64::MAX, &mut hit_record)
		{
			if depth == 0 {
				return Vector3::new(0.0, 0.0, 0.0);
			}

			// Hack
			let c = self.color(&new_ray, depth - 1);

			(1.0 - hit_record.metalness) * hit_record.albedo + hit_record.metalness * c
		} else {
			self.skybox(&ray)
		}
	}

	fn skybox(&self, ray: &Ray) -> Vector3<f64> {
		let t = 0.5 * (1.0 + ray.direction.normalize().y);
		(1.0 - t) * Vector3::new(1.0, 1.0, 1.0) + t * Vector3::new(0.5, 0.7, 1.0)
	}

	pub fn render<F>(
		&self,
		width: u32,
		height: u32,
		camera: &Camera,
		filename: String,
		on_render_column: F,
	) where
		F: Fn(),
	{
		let mut image = image::DynamicImage::new_rgb8(width, height);

		let samples = 200;
		let max_bounces = 16;
		let mut generator = rand::thread_rng();

		for x in 0..width {
			for y in 0..height {
				let mut color_accumulator = Vector3::new(0.0, 0.0, 0.0);
				for _i in 0..samples {
					let ray = camera.get_ray(
						(x as f64 + generator.gen::<f64>()) / width as f64,
						(y as f64 + generator.gen::<f64>()) / height as f64,
					);

					color_accumulator += self.color(&ray, max_bounces - 1);
				}
				color_accumulator /= samples as f64;
				image.put_pixel(
					x,
					height - 1 - y,
					image::Rgba([
						(color_accumulator.x * 255.0) as u8,
						(color_accumulator.y * 255.0) as u8,
						(color_accumulator.z * 255.0) as u8,
						255 as u8,
					]),
				);
			}
			on_render_column();
		}

		image.save(filename).unwrap();
	}
}
