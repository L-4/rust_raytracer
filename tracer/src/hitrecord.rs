use cgmath::Vector3;

pub struct HitRecord {
	pub distance: f64,
	pub position: Vector3<f64>,
	pub normal: Vector3<f64>,
	pub albedo: Vector3<f64>,
	pub metalness: f64,
}

impl HitRecord {
	pub fn new() -> HitRecord {
		HitRecord {
			distance: 0.0,
			position: Vector3::new(0.0, 0.0, 0.0),
			normal: Vector3::new(0.0, 0.0, 0.0),
			albedo: Vector3::new(1.0, 1.0, 1.0),
			metalness: 0.0,
		}
	}
}
