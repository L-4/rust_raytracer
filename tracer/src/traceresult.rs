use cgmath::Vector3;

#[derive(Copy, Clone)]
pub struct TraceResult {
	pub normal: Vector3<f64>,
	pub distance: f64,
	pub position: Vector3<f64>,
}
