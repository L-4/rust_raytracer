pub mod sphere;
//pub mod hitablelist;
use crate::material::Material;
use crate::ray::Ray;
use crate::traceresult::TraceResult;

/// Defines how to query for an intersection with a struct
pub trait Hitable {
	/// Called with a ray to determine whether the ray hit or not
	fn hit(&self, ray: &Ray, distance_min: f64, distance_max: f64) -> Option<TraceResult>;
	fn material(&self) -> &Material;
}
