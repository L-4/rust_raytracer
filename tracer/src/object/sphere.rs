use cgmath::{InnerSpace, Vector3};

use crate::material::Material;
use crate::object::Hitable;
use crate::ray::Ray;
use crate::traceresult::TraceResult;

pub struct Sphere {
	origin: Vector3<f64>,
	radius: f64,
	material: Material,
}

impl Sphere {
	pub fn new(origin: Vector3<f64>, radius: f64, material: Material) -> Sphere {
		Sphere {
			origin,
			radius,
			material,
		}
	}
}

impl Hitable for Sphere {
	// TODO: cleanup
	fn hit(&self, ray: &Ray, distance_min: f64, distance_max: f64) -> Option<TraceResult> {
		let delta = ray.origin - self.origin; // Vector from sphere origin to ray origin
		let a = ray.direction.dot(ray.direction); //
		let b = delta.dot(ray.direction);
		let c = delta.dot(delta) - self.radius * self.radius;
		let discriminant = b * b - a * c;
		if discriminant > 0.0 {
			let mut distance = (-b - (b * b - a * c).sqrt()) / a;
			if distance < distance_max && distance > distance_min {
				let position = ray.point_at_parameter(distance);
				return Some(TraceResult {
					distance,
					normal: (position - self.origin) / self.radius,
					position,
				});
			}

			distance = (-b + (b * b - a * c).sqrt()) / a;
			if distance < distance_max && distance > distance_min {
				let position = ray.point_at_parameter(distance);
				return Some(TraceResult {
					distance,
					normal: (position - self.origin) / self.radius,
					position,
				});
			}
		}
		None
	}

	fn material(&self) -> &Material {
		&self.material
	}
}
