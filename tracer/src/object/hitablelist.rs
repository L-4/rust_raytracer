use crate::hitrecord::HitRecord;
use crate::object::Hitable;
use crate::ray::Ray;

use cgmath::prelude::InnerSpace;
use cgmath::Vector3;

use rand::Rng;

/// A list of structs with the Hitable Trait
pub struct HitableList {
	list: Vec<Box<Hitable>>,
	hit_record: HitRecord,
}

impl HitableList {
	/// Instanciate new list
	pub fn new(hit_record: HitRecord) -> HitableList {
		HitableList {
			list: Vec::new(),
			hit_record,
		}
	}

	/// Push struct with hitable trait to list
	pub fn push<T: Hitable + 'static>(&mut self, item: T) {
		self.list.push(Box::new(item));
	}

	/// Query ray against all structs in list
	pub fn hit(
		&self,
		ray: &Ray,
		distance_min: f64,
		distance_max: f64,
		mut hit_record: &mut HitRecord,
	) -> Option<Ray> {
		let mut hit_object: Option<&Box<Hitable>> = None;
		let mut closest_so_far = distance_max;
		for hitable in &self.list {
			if hitable.hit(&ray, distance_min, closest_so_far) {
				hit_object = Some(hitable);
				closest_so_far = hit_record.distance;
			}
		}
		if let Some(object) = hit_object {
			let mut generator = rand::thread_rng();
			#[allow(unused_assignments)]
			let mut random_in_unit_sphere: Vector3<f64> = Vector3::new(0.0, 0.0, 0.0);
			loop {
				random_in_unit_sphere = 2.0
					* Vector3::new(
						generator.gen::<f64>(),
						generator.gen::<f64>(),
						generator.gen::<f64>(),
					) - Vector3::new(1.0, 1.0, 1.0);

				if random_in_unit_sphere.magnitude() < 1.0 {
					break;
				}
			}

			hit_record.metalness = object.material().metalness;
			let reflection =
				ray.direction - (2.0 * ray.direction.dot(hit_record.normal)) * hit_record.normal;

			Some(Ray::new(
				hit_record.position,
				reflection + random_in_unit_sphere * (1.0 - object.material().roughness),
			))
		//random_in_unit_sphere * (1.0 - object.material().roughness) + hit_record.normal))
		} else {
			None
		}
	}
}
